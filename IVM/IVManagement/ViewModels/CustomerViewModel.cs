namespace IVManagment.ViewModels
{
	
    using System;
    using IVManagement.ViewModels.Base;
    using System.Linq;
    using IVManagement.Data;
    using System.Collections.ObjectModel;
    using System.Windows.Documents;
    using System.Collections.Generic;
	
	public class CustomerViewModel : ViewModel
	{
		#region private proprieties
		
		public Customers currentCustomer {get; set;}
		private ObservableCollection<Customers> CustomersList;
		#endregion

		#region public proprieties

        public DBInventoryEntities db;
		#endregion

		#region Constructor

        public CustomerViewModel()
        {
            currentCustomer = new Customers();
        }
				
		#endregion

		#region others proprieties
		
		public string CustomerTitle
		{
			get {return currentCustomer.Title;}
			set
			{
				currentCustomer.Title = value;
				NotifyPropertyChanged("CustomerTitle");
			}
		}
		
		public string FirstName
		{
			get {return currentCustomer.Firstname;}
			set
			{
				currentCustomer.Firstname = value;
				NotifyPropertyChanged("FirstName");
			}
		}
		
		public string LastName
		{
			get {return currentCustomer.Lastname;}
			set
			{
				currentCustomer.Lastname = value;
				NotifyPropertyChanged("LastName");
			}
		}		

		#endregion

		#region private methods
		#endregion

		#region public methods
		
		public void LoadCustomers()
		{
			List<Customers> customers = new List<Customers>();

            using (db = new DBInventoryEntities())
			{
				//loc_customers = from p in db.Customers where p.UserId == UserId select p;
				var loc_customers = from p in db.Customers select p;
				foreach(Customers cus in loc_customers)
				{
                    customers.Add(cus);
				}

                CustomersList = new ObservableCollection<Customers>(customers);
			}
		}
		#endregion

		#region commands
		#endregion
	}
}