namespace IVManagment.ViewModels
{
    using IVManagement.Data;
    using IVManagement.Helps;
    using IVManagement.ViewModels.Base;
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel.DataAnnotations;
    using System.Windows;
    using System.Windows.Input;
    using IVManagement.Views;
    using System.Security;
    using System.Windows.Controls;
    using System.Windows.Interop;

    public class UserViewModel : ViewModel
    {

        #region privat properties

        private Users selectedUser;
        private ObservableCollection<Users> Users;

        

        #endregion

        #region public properties

        //public RelayCommand LoginCommand { get; private set; }
        public DBInventoryEntities db;

        public LoginView login;

        #endregion

        #region constructor

        public UserViewModel(LoginView loginView)
        {
            login = loginView;
            selectedUser = new Users();

            LoadUsers();
        }

        #endregion

        #region get and set properties values

        [Display(Name = "Username")]
        [Required]
        [StringLength(20)]
        public string Username
        {
            get { return selectedUser.Username; }
            set
            {  
                selectedUser.Username = value;
                ValidateProperty(value);
                NotifyPropertyChanged("Username");
                NotifyPropertyChanged("CanExecuteLogin");
            }
        }

        public Users SelectedUser
        {
            get
            {
                return selectedUser;
            }
            set
            {
                selectedUser = value;
                NotifyPropertyChanged("SelectedUser");
            }
        }

        #endregion

        #region define another properties

        public bool CanExecuteLogin
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Username) || string.IsNullOrWhiteSpace(login.txtPassword.Password))
                    return false;

                return true;
            }
        }

        #endregion

        #region private methods

        private void Login()
        {
            using (db = new DBInventoryEntities())
            {
                Owners owner = new Owners();

                try
                {
                    Users user = (from u in db.Users
                              where u.Username == Username && u.Password == login.txtPassword.Password
                              select u).First();

                    if (user != null)
                    {
                        owner = (from o in db.Owners where o.UserId == user.UserId select o).First();
                        MainWindow dashboard = new MainWindow(owner);
                        dashboard.Show();
                        login.Close();
                    }
                }
                catch(Exception ex)
                {
                    MessageBox.Show("Wrong username or password" + ex.ToString());
                    //Username = "";
                    login.txtPassword.Password = "";
                }
            }
        }

        private void Registration()
        {
            RegisterView Register = new RegisterView();
            login.Close();
            Register.Show();
        }

        #endregion

        private void CancelLogin()
        {
            login.Close();
        }
        #region public methods

        public void LoadUsers()
        {
            List<Users> userlist = new List<Users>();

            using (db = new DBInventoryEntities())
            {

                userlist = db.Users.ToList();
                Users = new ObservableCollection<Users>(userlist);

            }

        }

        #endregion

        #region commands

        public ICommand LoginCommand
        {
            get
            {
                return new RelayCommand(p => Login(),
                                        p => CanExecuteLogin);
            }
        }

        public ICommand RegisterCommand
        {
            get
            {
                return new RelayCommand(p => Registration());
            }
        }

        public ICommand CancelCommand
        {
            get
            {
                return new RelayCommand(p => CancelLogin());
            }
        }

        #endregion
    }
}