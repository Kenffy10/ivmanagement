﻿using IVManagement.Data;
using IVManagement.Helps;
using IVManagement.ViewModels.Base;
using Microsoft.Win32;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Threading.Tasks;
using System.Windows.Input;
using IVManagement.Views;
using System.Windows.Media.Imaging;
using System.Security.Policy;
using System.Collections.ObjectModel;

namespace IVManagement.ViewModels
{
    public class AddProductViewModel : ViewModel
    {
        #region private proprieties

        private string serialnumber { get; set; }
        private string productname { get; set; }
        private string productmarke { get; set; }
        private Nullable<int> quantity { get; set; }
        private Nullable<decimal> unitprice { get; set; }
        private string description { get; set; }
        private Nullable<decimal> inventoryprice { get; set; }
        private Nullable<decimal> saleprice { get; set; }
        public Nullable<System.DateTime> purchasedate { get; set; }
        private Nullable<int> startquantity { get; set; }
        public Nullable<int> statusquantity { get; set; }
        private Nullable<int> statusorders { get; set; }

        private string categoryname { get; set; }

        private string filelocation { get; set; }

        private string stockNotification { get; set; }
        private string notificationColor { get; set; }

        private string firstname { get; set; }
        private string lastname { get; set; }
        private string companyname { get; set; }

        private string stockname { get; set; }

        private string streetname { get; set; }
        private string housenumber { get; set; }
        private string postalcode { get; set; }
        private string city { get; set; }
        private string country { get; set; }

        private ObservableCollection<Stocks> stockList { get; set; }

        #endregion

        #region public proprieties
        public AddProductView AddView;
        public DBInventoryEntities db;
        #endregion

        #region Constructor

        public AddProductViewModel(AddProductView addView)
        {
            AddView = addView;
            LoadData();
        }
        #endregion

        #region others proprieties

        // Products attributes

        public string SerialNumber
        {
            get { return serialnumber; }
            set
            {
                serialnumber = value;
                NotifyPropertyChanged("SerialNumber");
            }
        }

        public string ProductName
        {
            get { return productname; }
            set
            {
                productname = value;
                NotifyPropertyChanged("Productname");
            }
        }

        public string ProductMarke
        {
            get { return productmarke; }
            set
            {
                productmarke = value;
                NotifyPropertyChanged("Productmarke");
            }
        }

        public string Description
        {
            get { return description; }
            set
            {
                description = value;
                NotifyPropertyChanged("Description");
            }
        }

        public Nullable<int> ProductQuantity
        {
            get { return quantity; }
            set
            {
                quantity = value;
                NotifyPropertyChanged("ProductQuantity");
                NotifyPropertyChanged("InventoryPrice");
            }
        }

        public Nullable<decimal> UnitPrice
        {
            get { return unitprice; }
            set
            {
                unitprice = value;
                NotifyPropertyChanged("UnitPrice");
                NotifyPropertyChanged("InventoryPrice");
            }
        }

        public Nullable<decimal> InventoryPrice
        {
            get 
            {
                return ProductQuantity * UnitPrice; 
            }
            set
            {
                inventoryprice = value;
                NotifyPropertyChanged("InventoryPrice");
            }
        }

        public Nullable<decimal> Saleprice
        {
            get { return saleprice; }
            set
            {
                saleprice = value;
                NotifyPropertyChanged("Saleprice");
            }
        }

        public Nullable<System.DateTime> PurchaseDate
        {
            get { return purchasedate; }
            set
            {
                purchasedate = value;
                NotifyPropertyChanged("Purchasedate");
            }
        }

        public Nullable<int> Startquantity
        {
            get { return startquantity; }
            set
            {
                startquantity = value;
                NotifyPropertyChanged("StockStatus");
            }
        }

        public Nullable<int> Statusquantity
        {
            get { return statusquantity; }
            set
            {
                statusquantity = value;
                NotifyPropertyChanged("Statusquantity");
            }
        }

        public Nullable<int> Statusorders
        {
            get { return statusorders; }
            set
            {
                statusorders = value;
                NotifyPropertyChanged("Statusorders");
            }
        }

        public string CategoryName
        {
            get { return categoryname; }
            set
            {
                categoryname = value;
                NotifyPropertyChanged("CategoryName");
            }
        }

        // Supplier

        public string FirstName
        {
            get { return firstname; }
            set
            {
                firstname = value;
                NotifyPropertyChanged("FirstName");
            }
        }

        public string LastName
        {
            get { return lastname; }
            set
            {
                lastname = value;
                NotifyPropertyChanged("LastName");
            }
        }

        public string CompanyName
        {
            get { return companyname; }
            set
            {
                companyname = value;
                NotifyPropertyChanged("CompanyName");
            }
        }

        public string FileLocation
        {
            get { return filelocation; }
            set
            {
                filelocation = value;
                NotifyPropertyChanged("FileLocation");
            }
        }

        public string Stockname
        {
            get { return stockname; }
            set
            {
                stockname = value;
                NotifyPropertyChanged("Stockname");
            }
        }

        public string StockNotification
        {
            get { return stockNotification; }
            set
            {
                stockNotification = value;
                NotifyPropertyChanged("StockNotification");
            }
        }

        public string NotificationColor
        {
            get { return notificationColor; }
            set
            {
                notificationColor = value;
                NotifyPropertyChanged("NotificationColor");
            }
        }
        // Address attribute

        public string StreetName
        {
            get { return streetname; }
            set
            {
                streetname = value;
                NotifyPropertyChanged("StreetName");
            }
        }

        public string HouseNumber
        {
            get { return housenumber; }
            set
            {
                housenumber = value;
                NotifyPropertyChanged("HouseNumber");
            }
        }

        public string PostalCode
        {
            get { return postalcode; }
            set
            {
                postalcode = value;
                NotifyPropertyChanged("PostalCode");
            }
        }

        public string City
        {
            get { return city; }
            set
            {
                city = value;
                NotifyPropertyChanged("City");
            }
        }

        public string Country
        {
            get { return country; }
            set
            {
                country = value;
                NotifyPropertyChanged("Country");
            }
        }

        public ObservableCollection<Stocks> StockList
        {
            get { return stockList; }
            set
            {
                stockList = value;
                NotifyPropertyChanged("StockList");
            }
        }


        public bool IsAddCommandValid
        {
            get
            {
                if (string.IsNullOrEmpty(ProductName)
                    //||	string.IsNullOrEmpty(ProductQuantity)
                    //||	string.IsNullOrEmpty(UnitPrice)
                  )
                    return false;

                return true;
            }
        }
        #endregion

        #region private methods

        private void AddProductInStock()
        {
           using (db = new DBInventoryEntities())
           {
               try
               {
                   // Add Supplier

                   Address address = new Address()
                   {
                       Streetname = StreetName,
                       Housenumber = HouseNumber,
                       Postalcode = PostalCode,
                       City = City,
                       Country = Country
                   };

                   db.Address.Add(address);
                   db.SaveChanges();

                   Suppliers supplier = new Suppliers()
                   {
                       Firstname = FirstName,
                       Lastname = LastName,
                       Companyname = CompanyName,
                       AddressId = address.AddressId
                   };

                   db.Suppliers.Add(supplier);
                   db.SaveChanges();

                   // Add Product

                   Categories category = new Categories()
                   {
                       Categoryname = CategoryName
                   };

                   db.Categories.Add(category);
                   db.SaveChanges();

                   Stocks stock = (from st in db.Stocks where st.OwnerId == AddView.Owner.OwnerId && st.Stockname == Stockname select st).First();

                   // Add picture

                   byte[] image = null;
                   FileStream fs = new FileStream(filelocation, FileMode.Open, FileAccess.Read);
                   BinaryReader br = new BinaryReader(fs);
                   image = br.ReadBytes((int)fs.Length);

                   Pictures productImage = new Pictures()
                   {
                       Filelocation = filelocation,
                       Productimage = image
                   };

                   db.Pictures.Add(productImage);
                   db.SaveChanges();

                   Products product = new Products()
                   {
                       StockId = stock.StockId,
                       CategoryId = category.CategoryId,
                       PictureId = productImage.PictureId,
                       Serialnumber = SerialNumber,
                       Productname = ProductName,
                       Productmarke = ProductMarke,
                       Description = Description,
                       Quantiy = ProductQuantity,
                       Unitprice = UnitPrice,
                       Inventoryprice = UnitPrice * ProductQuantity,
                       Purchasedate = DateTime.Now,
                       Startquantity = ProductQuantity,
                       Statusquantity = ProductQuantity,
                   };

                   db.Products.Add(product);
                   db.SaveChanges();

                   Purchases purchase = new Purchases()
                   {
                       SupplierId = supplier.SupplierId,
                       ProductId = product.ProductId,
                       Amount = UnitPrice * ProductQuantity,
                       Purchasedate = DateTime.Now
                   };

                   db.Purchases.Add(purchase);
                   db.SaveChanges();

                   StockNotification = "Stock successfully stored!";
                   NotificationColor = "green";

               }
               catch(Exception ex)
               {
                   MessageBox.Show(ex.ToString());

                   StockNotification = "Stock couldn't stored!";
                   NotificationColor = "red";
               }
           }
        }

        private void CancelAddProduct()
        {
            AddView.Close();
        }

        private void LoadPicture()
        {
            OpenFileDialog Dlg = new OpenFileDialog();
            Dlg.Filter = "Products pictures(*.png)| *png|jpg files(*.jpg)|*.jpg|All files(*.*)|*.*";
            if (Dlg.ShowDialog() == true)
            {
                filelocation = Dlg.FileName.ToString();
                AddView.ProductImage.Source = new BitmapImage(new Uri(Dlg.FileName));
            }
        }
        #endregion

        #region public methods

        public void LoadData()
        {
            using(db = new DBInventoryEntities())
            {
                // Load stock
                List<Stocks> loc_stocks = new List<Stocks>();

                loc_stocks = (from st in db.Stocks where st.OwnerId == AddView.Owner.OwnerId select st).ToList();
                StockList = new ObservableCollection<Stocks>(loc_stocks);
            }
        }
        #endregion

        #region commands

        public ICommand CancelProductCommand
        {
            get
            {
                return new RelayCommand(p => CancelAddProduct());
            }
        }

        public ICommand AddProductsCommand
        {
            get
            {
                return new RelayCommand(p => AddProductInStock(),
                                        p => IsAddCommandValid);
            }
        }

        public ICommand BrowseCommand
        {
            get
            {
                return new RelayCommand(p => LoadPicture());
            }
        }
        #endregion
    }
}
