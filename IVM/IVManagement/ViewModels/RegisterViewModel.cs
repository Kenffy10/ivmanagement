﻿
namespace IVManagement.ViewModels
{
    using IVManagement.Views;
    using IVManagement.ViewModels.Base;
    using System;
    using System.Linq;
    using System.Windows.Input;
    using IVManagement.Helps;
    using IVManagement.Data;

    public class RegisterViewModel : ViewModel
    {
        #region private proprieties

        private string firstname { get; set; }
        private string lastname { get; set; }
        private string username { get; set; }
        private string title { get; set; }
        private string gender { get; set; }

        private bool buttonFIsChecked { get; set; }
        private bool buttonMIsChecked { get; set; }

        private string notification;
        private string notificationcolor;

        private RegisterView register;
        private LoginView loginView;

        #endregion

        #region public proprieties

        public DBInventoryEntities db;
        #endregion

        #region Constructor

        public RegisterViewModel(RegisterView regView)
        {
            register = regView;
            buttonFIsChecked = true;
        }
        #endregion

        #region others proprieties

        public string Firstname
        {
            get { return firstname; }
            set
            {
                firstname = value;
                NotifyPropertyChanged("Firstname");
            }
        }

        public string Lastname
        {
            get { return lastname; }
            set
            {
                lastname = value;
                NotifyPropertyChanged("Lastname");
            }
        }

        public string Username
        {
            get { return username; }
            set
            {
                username = value;
                NotifyPropertyChanged("Username");
            }
        }

        public string Title
        {
            get 
            {
                if (Gender == "Female")
                    title = "Mrs";
                else
                    title = "Mr";

                return title; 
            }
            set
            {
                title = value;
                NotifyPropertyChanged("Title");
            }
        }

        public string Gender
        {
            get { return gender; }
            set
            {
                gender = value;
                NotifyPropertyChanged("Gender");
                NotifyPropertyChanged("Title");
            }
        }

        public string Notification
        {
            get { return notification; }
            set
            {
                notification = value;
                NotifyPropertyChanged("notification");
            }
        }

        public string NotificationColor
        {
            get { return notificationcolor; }
            set
            {
                notificationcolor = value;
                NotifyPropertyChanged("NotificationColor");
            }
        }

        public bool ButtonFIsChecked
        {
            get { return buttonFIsChecked; }
            set
            {
                buttonFIsChecked = value;
                //PerformRadioBox();
                NotifyPropertyChanged("ButtonFIsChecked");
            }
        }

        public bool ButtonMIsChecked
        {
            get { return buttonMIsChecked; }
            set
            {
                buttonMIsChecked = value;
                //PerformRadioBox();
                NotifyPropertyChanged("ButtonMIsChecked");
            }
        }

        public string Password
        {
            get
            {
                return register.txtPassword.Password;
            }
        }

        public string ConfirmPassword
        {
            get
            {
                return register.txtConfirmPassword.Password;
            }
        }

        public bool IsSingUpCommandValid
        {
            get
            {
                if ((string.IsNullOrEmpty(Firstname)
                          || string.IsNullOrEmpty(Lastname)
                          || string.IsNullOrEmpty(Username)
                          || string.IsNullOrEmpty(Gender)
                          || string.IsNullOrEmpty(Password)
                          || string.IsNullOrEmpty(ConfirmPassword)
                        )
                     && (Password != ConfirmPassword)
                  )
                {
                    return false;
                }
                else
                {
                    return true;
                }

            }
        }
        #endregion

        #region private methods
        #endregion

        #region public methods

        public void SignUp()
        {
            using (db = new DBInventoryEntities())
            {
                try
                {
                    Users loc_user = new Users
                    {
                        Username = Username,
                        Password = Password,
                        Usertype = "Customer"
                    };

                    db.Users.Add(loc_user);
                    db.SaveChanges();

                    Owners loc_customer = new Owners
                    {
                        Firstname = Firstname,
                        Lastname = Lastname,
                        UserId = loc_user.UserId
                    };

                    db.Owners.Add(loc_customer);
                    db.SaveChanges();

                    Notification = "Sign Up successfully !";
                    NotificationColor = "green";
                }
                catch (Exception ex)
                {
                    Notification = "Sign Up wrong!!!" + ex;
                    NotificationColor = "red";
                }
            }
        }

        public void SignUpCancel()
        {
            register.Close();
            loginView = new LoginView();
            loginView.Show();
        }

        public void Login()
        {
            loginView = new LoginView();
            loginView.Show();
            register.Close();
        }

        #endregion

        #region commands

        public ICommand SignUpCommand
        {
            get
            {
                return new RelayCommand(p => SignUp(),
                                        p => IsSingUpCommandValid);
            }
        }

        public ICommand CancelCommand
        {
            get
            {
                return new RelayCommand(p => SignUpCancel());
            }
        }

        public ICommand LoginCommand
        {
            get
            {
                return new RelayCommand(p => Login());
            }
        }
        #endregion

    }
}
