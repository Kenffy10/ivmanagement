
namespace IVManagment.ViewModels
{
    using IVManagement.Data;
    using IVManagement.Helps;
    using IVManagement.Views;
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Windows;
    using System.Windows.Input;
    using IVManagement.ViewModels.Base;
    using IVManagement.Models;
	
	public class MainWindowViewModel : ViewModel
	{
		#region private proprieties

        private string ownername { get; set; }
        private string companyname { get; set; }

        private List<string> searchfilter { get; set; }

        private string userpic { get; set; }

        private Nullable<decimal> stockvalue { get; set; }
        private Nullable<decimal> dailyrecipe { get; set; }
        private int numberofproducts { get; set;}
        private int numberoforders { get; set; }
        private int numberofsales { get; set; }

        private Products selectedProduct { get; set; }
        private ObservableCollection<Products> productsList { get; set; }
        private ObservableCollection<Products> lastaddedproductsList { get; set; }
        private ObservableCollection<Orders> ordersList { get; set; }

        private Owners Owner { get; set; }

        private DBHandle handle { get; set; }
        private ItemTemplate selectedItem { get; set; }
        private ItemTemplate selectedDItem { get; set; }
        private Orders selectedOrder { get; set; }
        private Address selectedAddress { get; set; }
        private Customers selectedCustomer { get; set; }
        private ObservableCollection<ItemTemplate> itemList { get; set; }
        private ObservableCollection<ItemTemplate> ditemList { get; set; }

        private string productname { get; set; }

        private DateTime currentDate { get; set; }
		#endregion

		#region public proprieties

        public DBInventoryEntities db;
		#endregion

		#region Constructor

        public MainWindowViewModel(Owners owner)
        {
            Owner = new Owners();
            Owner = owner;
            searchfilter = new List<string>();
            currentDate = DateTime.Now;
            handle = new DBHandle();
            userpic = "/Images/tempuserpic.png";
            ProductsList = new ObservableCollection<Products>();
            LastaddedproductsList = new ObservableCollection<Products>();
            ordersList = new ObservableCollection<Orders>();

            selectedItem = new ItemTemplate();
            selectedDItem = new ItemTemplate();
            selectedOrder = new Orders();
            selectedCustomer = new Customers();
            selectedAddress = new Address();
            itemList = new ObservableCollection<ItemTemplate>();
            ditemList = new ObservableCollection<ItemTemplate>();

            LoadData();
        }

		
		#endregion

		#region others proprieties

        public DateTime CurrentDate
        {
            get { return currentDate; }
            set
            {
                currentDate = value;
                NotifyPropertyChanged("Searchfilter");
            }
        }

        public List<string> Searchfilter
        {
            get { return searchfilter; }
            set
            {
                searchfilter = value;
                NotifyPropertyChanged("Searchfilter");
            }
        }

        public string UserPic
        {
            get { return userpic; }
            set
            {
                userpic = value;
                NotifyPropertyChanged("UserPic");
            }
        }

        public string OwnerName
        {
            get { return Owner.Firstname + " " + Owner.Lastname; }
        }

        public string CompanyName
        {
            get { return Owner.Companyname; }
        }

        public string Productname
        {
            get { return productname; }
            set
            {
                productname = value;
                NotifyPropertyChanged("Productname");
            }
        }

        public DBHandle Handle
        {
            get { return handle; }
            set
            {
                handle = value;
                NotifyPropertyChanged("Handle");
            }
        }

        public Products SelectedProduct
        {
            get { return selectedProduct; }
            set
            {
                selectedProduct = value;
                NotifyPropertyChanged("SelectedProduct");
            }
        }

        public Nullable<decimal> StockValue
        {
            get { return stockvalue; }
            set
            {
                stockvalue = value;
                NotifyPropertyChanged("StockValue");
            }
        }

        public Nullable<decimal> DailyRecipe
        {
            get { return dailyrecipe; }
            set
            {
                dailyrecipe = value;
                NotifyPropertyChanged("DailyRecipe");
            }
        }

        public int NumberOfproducts
        {
            get { return numberofproducts; }
            set
            {
                numberofproducts = value;
                NotifyPropertyChanged("NumberOfproducts");
            }
        }

        public int NumberOforders
        {
            get { return numberoforders; }
            set
            {
                numberoforders = value;
                NotifyPropertyChanged("NumberOforders");
            }
        }

        public int NumberOfsales
        {
            get { return numberofsales; }
            set
            {
                numberofsales = value;
                NotifyPropertyChanged("NumberOfsales");
            }
        }

        public ObservableCollection<Products> ProductsList
        {
            get { return productsList; }
            set
            {
                productsList = value;
                NotifyPropertyChanged("ProductsList");
            }
        }

        public ObservableCollection<Products> LastaddedproductsList
        {
            get { return lastaddedproductsList; }
            set
            {
                lastaddedproductsList = value;
                NotifyPropertyChanged("LastaddedproductsList");
            }
        }

        public ObservableCollection<Orders> OrderList
        {
            get { return ordersList; }
            set
            {
                ordersList = value;
                NotifyPropertyChanged("OrderList");
            }
        }

        public Nullable<decimal> SalePrice
        {
            get { return selectedItem.Saleprice; }
            set
            {
                selectedItem.Saleprice = value;
                NotifyPropertyChanged("SalePrice");
            }
        }

        public ItemTemplate SelectedItem
        {
            get { return selectedItem; }
            set
            {
                selectedItem = value;
                NotifyPropertyChanged("SelectedItem");
            }
        }

        public ItemTemplate SelectedDItem
        {
            get { return selectedDItem; }
            set
            {
                selectedDItem = value;
                NotifyPropertyChanged("SelectedDItem");
            }
        }

        public Orders SelectedOrder
        {
            get 
            {
                if (selectedOrder.Orderquantity != null)
                    selectedOrder.Amount = SelectedItem.Unitprice * selectedOrder.Orderquantity;
                return selectedOrder;                
            }
            set
            {
                selectedOrder = value;
                NotifyPropertyChanged("SelectedOrder");
                //NotifyPropertyChanged("OrderAmount");
            }
        }

        public Address SelectedAddress
        {
            get { return selectedAddress; }
            set
            {
                selectedAddress = value;
                NotifyPropertyChanged("SelectedAddress");
            }
        }

        public Customers SelectedCustomer
        {
            get { return selectedCustomer; }
            set
            {
                selectedCustomer = value;
                NotifyPropertyChanged("SelectedCustomer");
            }
        }

        public Nullable<decimal> OrderAmount
        {
            get 
            {
                if (SelectedOrder.Orderquantity != null)
                {
                    SelectedOrder.Amount = SelectedItem.Unitprice * SelectedOrder.Orderquantity;
                    return SelectedOrder.Amount;
                }
                else
                {
                    return null;
                }
                
            }
            set
            {
                selectedOrder.Amount = value;
                NotifyPropertyChanged("OrderAmount");
            }
        }

        public Nullable<decimal> OrderVersement
        {
            get
            {
                return SelectedOrder.Versement;
            }
            set
            {
                SelectedOrder.Versement = value;
                NotifyPropertyChanged("OrderVersement");
                NotifyPropertyChanged("SelectedOrder");
            }
        }

        public ObservableCollection<ItemTemplate> ItemList
        {
            get { return itemList; }
            set
            {
                itemList = value;
                NotifyPropertyChanged("ItemList");
            }
        }

        public ObservableCollection<ItemTemplate> DItemList
        {
            get { return ditemList; }
            set
            {
                ditemList = value;
                NotifyPropertyChanged("DItemList");
            }
        }

        public bool IsSaveOrderActiv
        {
            get
            {
                if (SelectedOrder.Orderquantity != null)
                    return true;
                return false;
            }
        }

        public bool IsPayOrderActiv
        {
            get
            {
                if   ((SelectedOrder.Orderquantity != null)
                  && ((SelectedOrder.Versement != null) 
                  && (SelectedOrder.Versement >= selectedOrder.Amount)))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

		#endregion

		#region private methods

        private void CloseSession()
        {
            MessageBoxResult msg = MessageBox.Show("Do you really want to close the App?", "Delete Stock", MessageBoxButton.YesNo);
            if (msg == MessageBoxResult.Yes)
            {
            }
            else if (msg == MessageBoxResult.No)
            {
                //do nothing !!!
            }
        }

        private object DeleteProduct()
        {
            throw new NotImplementedException();
        }

        private object OpenDetailsProduct()
        {
            throw new NotImplementedException();
        }

        private object EditProduct()
        {
            throw new NotImplementedException();
        }

        private void SaveSalePrice()
        {
            Products item = new Products();

            using (db = new DBInventoryEntities())
            {
                item = (from p in db.Products
                            where p.StockId == SelectedItem.StockId && p.Productname == SelectedItem.Productname
                            select p).First();
                if(item != null)
                {
                    item.Saleprice = SalePrice;
                    db.SaveChanges();
                }
                
            }
        }

        private void SaveOrders()
        {
            using (db = new DBInventoryEntities())
            {
                // Store customer address
                Address address = new Address()
                {
                    Streetname = SelectedAddress.Streetname,
                    Housenumber = SelectedAddress.Housenumber,
                    Postalcode = SelectedAddress.Postalcode,
                    City = SelectedAddress.City,
                    Country = SelectedAddress.Country
                };

                db.Address.Add(address);
                db.SaveChanges();

                // Store customer
                Customers customer = new Customers()
                {
                    Title = SelectedCustomer.Title,
                    Firstname = SelectedCustomer.Firstname,
                    Lastname = SelectedCustomer.Lastname,
                    AddressId = address.AddressId
                };

                db.Customers.Add(customer);
                db.SaveChanges();

                // Store order
                Orders order = new Orders()
                {
                    CustomerId = customer.CustomerId,
                    ProductId = SelectedItem.ProductId,
                    Orderquantity = SelectedOrder.Orderquantity,
                    Orderdate = DateTime.Now,
                    Shipvia = null,
                    Amount = SelectedOrder.Amount,
                    Versement = OrderVersement,
                    Paystatus = null,
                    AddressId = address.AddressId
                };

                db.Orders.Add(order);
                db.SaveChanges();
            }
        }

        private void PayOrders()
        {
            using (db = new DBInventoryEntities())
            {

            }
        }

		#endregion

		#region public 

        public void LoadData()
        {
            List<Products> loc_product = new List<Products>(); ;
            List<Orders> orderlist = new List<Orders>();
            List<ItemTemplate> lastproduct = new List<ItemTemplate>();
            List<Sales> loc_sale = new List<Sales>();
            List<Stocks> loc_stock = new List<Stocks>();

            // List<Products> loc_product = new List<Products>();
            // List<Stocks> loc_stock = new List<Stocks>();

            List<ItemTemplate> loc_test = new List<ItemTemplate>();
            

            ProductsList = new ObservableCollection<Products>();

            using (db = new DBInventoryEntities())
            {
                // get stock List

                loc_stock = (from st in db.Stocks where st.OwnerId == Owner.OwnerId select st).ToList();

                // get number of products

                foreach(Stocks st in loc_stock)
                {
                    var items = (from p in db.Products
                                   where p.StockId == st.StockId
                                   select p).ToList();

                    foreach(Products p in items)
                    {
                        loc_product.Add(p);
                    }

                }
                ProductsList = new ObservableCollection<Products>(loc_product);
                NumberOfproducts = ProductsList.Count();


                foreach (Stocks st in loc_stock)
                {
                    var loc_prod = (from p in db.Products
                                    join pic in db.Pictures on p.PictureId equals pic.PictureId
                                    where p.StockId == st.StockId
                                    orderby p.ProductId descending 
                                    select new ItemTemplate
                                    {
                                        ProductId = p.ProductId,
                                        StockId = p.StockId,
                                        CategoryId = p.CategoryId,
                                        PictureId = p.PictureId,
                                        Picture = pic.Productimage,
                                        Serialnumber = p.Serialnumber,
                                        Productname = p.Productname,
                                        Productmarke = p.Productmarke,
                                        Description = p.Description,
                                        Quantity = p.Quantiy,
                                        Unitprice = p.Unitprice,
                                        Inventoryprice = p.Inventoryprice,
                                        Saleprice = p.Saleprice,
                                        Purchasedate = p.Purchasedate,
                                        Startquantity = p.Startquantity,
                                        Statusquantity = p.Statusquantity,
                                        Statusorder = p.Statusorders
                                    }).Take(20).ToList();
                    foreach (ItemTemplate p in loc_prod)
                    {
                        lastproduct.Add(p);
                    }
                }

                DItemList = new ObservableCollection<ItemTemplate>(lastproduct);

                // Evaluate stock value
                decimal stValue = new decimal();
                stValue = 0;

                foreach (Products pdt in ProductsList)
                {
                    stValue += pdt.Inventoryprice.Value;
                }
                StockValue = stValue;

                // Daily recipe
                foreach (Products p in ProductsList)
                {
                    loc_sale = (from s in db.Sales
                                join or in db.Orders on s.OrderId equals or.OrderId
                                where or.ProductId == p.ProductId
                                select s).ToList();
                }
                NumberOfsales = loc_sale.Count();


                decimal tmp = new decimal();
                tmp = 0;
                foreach (Products p in ProductsList)
                {
                    List<Orders> loc_order = new List<Orders>();

                    loc_order = (from o in db.Orders
                                 where o.ProductId == p.ProductId
                                 select o).ToList();

                    foreach (Orders order in loc_order)
                    {
                        orderlist.Add(order);
                    }                 
                }

                foreach (Orders order in orderlist)
                {
                    tmp += order.Amount.Value;
                } 
                DailyRecipe = tmp;
                NumberOforders = orderlist.Count();

                OrderList = new ObservableCollection<Orders>(orderlist);


                // List of object

                // get stock List

                 loc_stock = (from st in db.Stocks where st.OwnerId == Owner.OwnerId select st).ToList();
                //loc_stock = (from st in db.Stocks where st.OwnerId == Owner.OwnerId select st).ToList();

                // get number of products

                foreach (Stocks st in loc_stock)
                {
                    var items = (from p in db.Products
                                 where p.StockId == st.StockId
                                 select p).ToList();

                    foreach (Products p in items)
                    {
                        loc_product.Add(p);
                    }

                }
                ProductsList = new ObservableCollection<Products>(loc_product);

                foreach (Stocks st in loc_stock)
                {
                    var items = (from p in db.Products
                                 join pic in db.Pictures on p.PictureId equals pic.PictureId
                                 where p.StockId == st.StockId
                                 select new ItemTemplate
                                 {
                                     ProductId = p.ProductId,
                                     StockId = p.StockId,
                                     CategoryId = p.CategoryId,
                                     PictureId = p.PictureId,
                                     Picture = pic.Productimage,
                                     Serialnumber = p.Serialnumber,
                                     Productname = p.Productname,
                                     Productmarke = p.Productmarke,
                                     Description = p.Description,
                                     Quantity = p.Quantiy,
                                     Unitprice = p.Unitprice,
                                     Inventoryprice = p.Inventoryprice,
                                     Saleprice = p.Saleprice,
                                     Purchasedate = p.Purchasedate,
                                     Startquantity = p.Startquantity,
                                     Statusquantity = p.Statusquantity,
                                     Statusorder = p.Statusorders
                                 }).ToList();

                    foreach (ItemTemplate p in items)
                    {
                        loc_test.Add(p);
                    }
                }

                ItemList = new ObservableCollection<ItemTemplate>(loc_test);

                Searchfilter.Add("refresh");

                Categories loc_category;
                foreach (Products pdt in ProductsList)
                {
                    loc_category = new Categories();
                    loc_category = (from c in db.Categories where c.CategoryId == pdt.CategoryId select c).First();

                    if (Searchfilter.Contains(loc_category.Categoryname))
                    {
                        // Do nothing !!!
                    }
                    else
                    {
                        Searchfilter.Add(loc_category.Categoryname);
                    }
                    
                }

            }
        }


        public void AddNewStock()
        {
            AddStockView stockview = new AddStockView(Owner);
            stockview.Show();
        }

        public void AddNewProduct()
        {
            AddProductView productview = new AddProductView(Owner);
            productview.Show();
        }

		#endregion

		#region commands

        public ICommand CloseCommand
        {
            get
            {
                return new RelayCommand(p => CloseSession());
            }
        }

        public ICommand EditProductCommand
        {
            get
            {
                return new RelayCommand(p => EditProduct());
            }
        }

        public ICommand DetailsProductCommand
        {
            get
            {
                return new RelayCommand(p => OpenDetailsProduct());
            }
        }

        public ICommand DeleteProductCommand
        {
            get
            {
                return new RelayCommand(p => DeleteProduct());
            }
        }


        // Purchases stocks commands

        public ICommand AddStockCommand
        {
            get
            {
                return new RelayCommand(p => AddNewStock());
            }
        }

        public ICommand AddProductCommand
        {
            get
            {
                return new RelayCommand(p => AddNewProduct());
            }
        }


        // Sale command


        public ICommand SaveSalePriceCommand
        {
            get
            {
                return new RelayCommand(p => SaveSalePrice());
            }
        }


        public ICommand SaveOrderCommand
        {
            get
            {
                return new RelayCommand(p => SaveOrders(),
                                        p => IsSaveOrderActiv);
            }
        }

        public ICommand PayOrderCommand
        {
            get
            {
                return new RelayCommand(p => PayOrders(),
                                        p => IsPayOrderActiv);
            }
        }

        ////////////////////////////////////////////////////////////
		#endregion	
	}
}

/*
#region private proprieties
#endregion

#region public proprieties
#endregion

#region Constructor
#endregion

#region others proprieties
#endregion

#region private methods
#endregion

#region public methods
#endregion

#region commands
#endregion
*/