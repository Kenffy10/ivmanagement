
namespace IVManagment.ViewModels
{
    using IVManagement.Data;
    using IVManagement.Helps;
    using IVManagement.ViewModels.Base;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows;
    using System.Windows.Input;
	public class ProductViewModel : ViewModel
	{
		#region private proprieties
		
		private ObservableCollection<Products> productsList;
		private Products selectedProduct;
        private int numberOfCategories;
        private int numberOfSales;
        private int numberOfPurchases;
        private int numberOfOrders;
        private int numberOfSuppliers;
		#endregion

		#region public proprieties

        public DBInventoryEntities db;
		public Window LoginView;
		#endregion

		#region Constructor
		
		public ProductViewModel()
		{
            //currentUserId = userid;
			//LoginView = loginView;
			selectedProduct = new Products();
            LoadProducts();
            LoadStatistics();
		}
		#endregion

		#region others proprieties
		
		public string SerialNumber
		{
			get{ return selectedProduct.Serialnumber; }
			set
			{
				selectedProduct.Serialnumber = value;
				NotifyPropertyChanged("SerialNumber");
			}
		}
		
		public string ProductName
		{
			get{ return selectedProduct.Productname; }
			set
			{
				selectedProduct.Productname = value;
				NotifyPropertyChanged("Productname");
			}
		}
		
		public string ProductMarke
		{
			get{ return selectedProduct.Productmarke; }
			set
			{
				selectedProduct.Productmarke = value;
				NotifyPropertyChanged("Productmarke");
			}
		}
		
		public string ProductDescription
		{
			get{ return selectedProduct.Description; }
			set
			{
				selectedProduct.Description = value;
				NotifyPropertyChanged("ProductDescription");
			}
		}

        public Nullable<int> ProductQuantity
		{
			get{ return selectedProduct.Quantiy; }
			set
			{
                selectedProduct.Quantiy = value;
				NotifyPropertyChanged("ProductQuantity");
			}
		}

        public Nullable<decimal> UnitPrice
		{
			get{ return selectedProduct.Unitprice; }
			set
			{
				selectedProduct.Unitprice = value;
				NotifyPropertyChanged("UnitPrice");
			}
		}

        public Nullable<decimal> InventoryPrice
		{
			get{ return selectedProduct.Inventoryprice; }
			set
			{
				selectedProduct.Inventoryprice = value;
				NotifyPropertyChanged("InventoryPrice");
			}
		}

        public Nullable<decimal> Saleprice
        {
            get { return selectedProduct.Saleprice; }
            set
            {
                selectedProduct.Saleprice = value;
                NotifyPropertyChanged("Saleprice");
            }
        }

        public Nullable<System.DateTime> Purchasedate
		{
            get { return selectedProduct.Purchasedate; }
			set
			{
                selectedProduct.Purchasedate = value;
                NotifyPropertyChanged("Purchasedate");
			}
		}

        public Nullable<int> Startquantity
		{
            get { return selectedProduct.Startquantity; }
			set
			{
                selectedProduct.Startquantity = value;
				NotifyPropertyChanged("StockStatus");
			}
		}

        public Nullable<int> Statusquantity
		{
            get { return selectedProduct.Statusquantity; }
			set
			{
                selectedProduct.Statusquantity = value;
				NotifyPropertyChanged("PurchaseStatus");
			}
		}

        public Nullable<int> Statusorders
		{
            get { return selectedProduct.Statusorders; }
			set
			{
                selectedProduct.Statusorders = value;
                NotifyPropertyChanged("Statusorders");
			}
		}
		
		public Products SelectedProduct
		{
			get{ return selectedProduct; }
			set
			{
				selectedProduct = value;
				NotifyPropertyChanged("SelectedProduct");
			}
		}
		
		public ObservableCollection<Products> ProductsList
		{
			get{ return productsList; }
			set
			{
				productsList = value;
				NotifyPropertyChanged("ProductsList");
			}
		}
		
		public bool IsAddCommandValid
		{
			get
			{
				if(		string.IsNullOrEmpty(ProductName)
				  //||	string.IsNullOrEmpty(ProductQuantity)
				  //||	string.IsNullOrEmpty(UnitPrice)
                  )
					return false;
				
				return true;
			}
		}
		
		public bool IsDeleteCommandValid
		{
			get
			{
				if(		string.IsNullOrEmpty(ProductName)
				  //||	string.IsNullOrEmpty(ProductQuantity)
				  //||	string.IsNullOrEmpty(UnitPrice)
				  )
					return false;
				
				return true;
			}
		}
		
		public bool IsUpdateCommandValid
		{
			get
			{
				if(		string.IsNullOrEmpty(ProductName)
				  //||	string.IsNullOrEmpty(ProductQuantity)
				  //||	string.IsNullOrEmpty(UnitPrice)
				  )
					return false;
				
				return true;
			}
		}
		#endregion

		#region private methods

        private Nullable<decimal> ComputeInventoryPrice()
		{
			return ProductQuantity * UnitPrice;
		}

        private Nullable<int> ComputeStockStatus()
		{
            return ProductQuantity - Statusquantity;
		}
		
		private int ComputePurchaseStatus()
		{
			List<Purchases> PurchasesList = new List<Purchases>();
            using (db = new DBInventoryEntities())
			{
				var loc_Purchases = from pur in db.Purchases where pur.ProductId == 2 select pur;
				foreach (Purchases pur in loc_Purchases)
				{
					PurchasesList.Add(pur);
				}
			}
			
			return PurchasesList.Count;
		}
		
		private int ComputeOrderStatus()
		{
            List<Orders> OrderList = new List<Orders>();
            using (db = new DBInventoryEntities())
			{
				var loc_Orders = from o in db.Orders where o.CustomerId == 2 select o;
				foreach (Orders ord in loc_Orders)
				{
                    OrderList.Add(ord);
				}
			}

            return OrderList.Count;
		}        

        public int NumberOfCategories
        {
            get { return numberOfCategories; }
            set
            {
                numberOfCategories = value;
                NotifyPropertyChanged("NumberOfCategories");
            }
        }

        public int NumberOfSales
        {
            get { return numberOfSales; }
            set
            {
                numberOfSales = value;
                NotifyPropertyChanged("NumberOfSales");
            }
        }

        public int NumberOfPurchases
        {
            get { return numberOfPurchases; }
            set
            {
                numberOfPurchases = value;
                NotifyPropertyChanged("NumberOfPurchases");
            }
        }

        public int NumberOfOrders
        {
            get { return numberOfOrders; }
            set
            {
                numberOfOrders = value;
                NotifyPropertyChanged("NumberOfOrders");
            }
        }

        public int NumberOfSuppliers
        {
            get { return numberOfSuppliers; }
            set
            {
                numberOfSuppliers = value;
                NotifyPropertyChanged("NumberOfCategories");
            }
        }
		#endregion

		#region public methods
		
		public void LoadProducts()
		{
            List<Products> productlist = new List<Products>();

            using (db = new DBInventoryEntities())
			{
                productlist = db.Products.ToList();
                ProductsList = new ObservableCollection<Products>(productlist);
			}
		}

        public void LoadStatistics()
        {
            using (db = new DBInventoryEntities())
            {
                numberOfCategories = db.Categories.Count();

                var tmpSales = from sale in db.Sales
                               join o in db.Orders on sale.OrderId equals o.OrderId select sale;
                numberOfSales = tmpSales.Count();

                numberOfSuppliers = db.Suppliers.Count();
                numberOfOrders = db.Orders.Count();
                numberOfPurchases = db.Purchases.Count();
            }
        }
		
		public void AddProductInStock()
		{
            using (db = new DBInventoryEntities())
			{
				Products products = new Products()
				{
					CategoryId = getCategoryId(),
					Serialnumber = SerialNumber,
					Productname = ProductName,
					Productmarke = ProductMarke,
					Description = ProductDescription,
					Quantiy = ProductQuantity,
					Unitprice = UnitPrice,
					Inventoryprice = ComputeInventoryPrice(),
					Purchasedate = DateTime.Now,
					Startquantity = ComputeStockStatus(),
					Statusquantity = ComputePurchaseStatus(),
					Statusorders = ComputeOrderStatus()
				};
				
				db.Products.Add(products);
                db.SaveChanges();
			}
		}       
		
		public void DeleteProductfromStock()
		{
            using (db = new DBInventoryEntities())
			{
				Products products = (from p in db.Products where p.CategoryId == 2 select p).First();
				db.Products.Remove(products);
                db.SaveChanges();
			}
		}

		public void UpdateProductInStock()
		{
            using (db = new DBInventoryEntities())
			{
                Products products = (from p in db.Products where p.CategoryId == 2 select p).First();
				products.CategoryId = getCategoryId();
				products.Serialnumber = SerialNumber;
				products.Productname = ProductName;
				products.Productmarke = ProductMarke;
				products.Description = ProductDescription;
				products.Quantiy = ProductQuantity;
				products.Unitprice = UnitPrice;
				products.Inventoryprice = ComputeInventoryPrice();
				products.Purchasedate = DateTime.Now;
                products.Startquantity = ComputeStockStatus();
                products.Statusquantity = ComputePurchaseStatus();
                products.Statusorders = ComputeOrderStatus();
                db.SaveChanges();
			}
		}

        private int? getCategoryId()
        {
            throw new NotImplementedException();
        }
		#endregion

		#region commands
		
		public ICommand AddProductsCommand
		{
			get
			{
				return new RelayCommand(p => AddProductInStock(),
										p => IsAddCommandValid);
			}
		}
		
		public ICommand DeleteProductsCommand
		{
			get
			{
				return new RelayCommand(p => DeleteProductfromStock(),
										p => IsDeleteCommandValid);
			}
		}
		
		public ICommand UpdateProductsCommand
		{
			get
			{
				return new RelayCommand(p => UpdateProductInStock(),
										p => IsUpdateCommandValid);
			}
		}
		#endregion
	}
}