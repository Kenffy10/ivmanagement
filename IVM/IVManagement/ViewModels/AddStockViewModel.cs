﻿using IVManagement.Data;
using IVManagement.Helps;
using IVManagement.ViewModels.Base;
using IVManagement.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace IVManagement.ViewModels
{
    public class AddStockViewModel : ViewModel
    {
        #region private proprieties

        private Stocks selectedStock { get; set; }
        private AddStockView stockview { get; set; }
        private string stockNotification { get; set; }
        private string notificationColor { get; set; }

        private ObservableCollection<Stocks> stocklist { get; set; }
        #endregion

        #region public proprieties

        public DBInventoryEntities db;

        #endregion

        #region Constructor

        public AddStockViewModel(AddStockView stkview)
        {
            selectedStock = new Stocks();
            stocklist = new ObservableCollection<Stocks>();
            stockview = stkview;
            LoadDataStocks();
        }
        #endregion

        #region others proprieties

        public string StockName
        {
            get { return selectedStock.Stockname; }
            set
            {
                selectedStock.Stockname = value;
                NotifyPropertyChanged("StockName");
            }
        }

        public string SerialNumber
        {
            get { return selectedStock.Serialnumber; }
            set
            {
                selectedStock.Serialnumber = value;
                NotifyPropertyChanged("SerialNumber");
            }
        }

        public string CapaCity
        {
            get { return selectedStock.Capacity; }
            set
            {
                selectedStock.Capacity = value;
                NotifyPropertyChanged("CapaCity");
            }
        }

        public string StockNotification
        {
            get { return stockNotification; }
            set
            {
                stockNotification = value;
                NotifyPropertyChanged("StockNotification");
            }
        }

        public string NotificationColor
        {
            get { return notificationColor; }
            set
            {
                notificationColor = value;
                NotifyPropertyChanged("NotificationColor");
            }
        }

        public Stocks SelectedStock
        {
            get 
            {
                return selectedStock; 
            }
            set
            {
                selectedStock = value;
                NotifyPropertyChanged("SelectedStock");
            }
        }


        public ObservableCollection<Stocks> StockList
        {
            get { return stocklist; }
            set
            {
                stocklist = value;
                NotifyPropertyChanged("StockList");
            }
        }

        public bool IsAddStockValid
        {
            get
            {
                return true;
               //if (     string.IsNullOrEmpty(StockName)
               //      || string.IsNullOrEmpty(SerialNumber)
               //      || string.IsNullOrEmpty(CapaCity))
               //{
               //    return false;
               //}
               //else
               //{
               //    return true;
               //}
            }
        }
        #endregion

        private object OpenDetailsStock()
        {
            throw new NotImplementedException();
        }
        #region private methods

        private void AddStock()
        {
            List<Stocks> loc_stocklist = new List<Stocks>();

            using (db = new DBInventoryEntities())
            {
                try
                {
                    Stocks currStock = new Stocks()
                    {
                        Stockname = StockName,
                        Serialnumber = SerialNumber,
                        Capacity = CapaCity,
                        OwnerId = stockview.Owner.OwnerId
                    };

                    db.Stocks.Add(currStock);
                    db.SaveChanges();

                    selectedStock = currStock;

                    StockNotification = "Stock successfully stored!";
                    NotificationColor = "green";
                    //System.Threading.Thread.Sleep(5000);
                    //StockNotification = null;
                }
                catch(Exception ex)
                {
                    StockNotification = "Stock couldn't stored!" + ex;
                    NotificationColor = "red";
                    //System.Threading.Thread.Sleep(5000);
                    //StockNotification = "";
                }
            }

            LoadDataStocks();
        }

        private void CancelAddStock()
        {
            stockview.Close();
        }
        #endregion

        #region public methods

        public void LoadDataStocks()
        {
            //selectedStock = new Stocks();
            List<Stocks> loc_stocklist = new List<Stocks>();

            using (db = new DBInventoryEntities())
            {
                loc_stocklist = (from s in db.Stocks where s.OwnerId == stockview.Owner.OwnerId select s).ToList();
                StockList = new  ObservableCollection<Stocks>(loc_stocklist);
            }
        }

        public void DeleteStock()
        {
            using (db = new DBInventoryEntities())
            {
                
                MessageBoxResult msg = MessageBox.Show("Delete Stock","Do you really want to delete de current stock?",MessageBoxButton.YesNo);
                if (msg == MessageBoxResult.Yes)
                {

                    if (selectedStock != null)
                    {
                        Stocks loc_Stock = new Stocks();
                        loc_Stock = (from st in db.Stocks where st.Stockname == selectedStock.Stockname && st.Serialnumber == selectedStock.Serialnumber select st).First();
                        db.Stocks.Remove(loc_Stock);
                        db.SaveChanges();
                    }

                    StockNotification = "Stock successfully deleted!";
                    NotificationColor = "green";
                    //System.Threading.Thread.Sleep(5000);
                    //StockNotification = null;
                }
                else if (msg == MessageBoxResult.No)
                {
                    StockNotification = "Stock couldn't deleted!";
                    NotificationColor = "red";
                    //System.Threading.Thread.Sleep(5000);
                    //StockNotification = "";
                }
            }
            LoadDataStocks();
        }
        #endregion

        #region commands

        public ICommand AddStockCommand
        {
            get
            {
                return new RelayCommand(p => AddStock(),
                                        p => IsAddStockValid);
            }
        }

        public ICommand CancelStockCommand
		{
			get
			{
				return new RelayCommand(p => CancelAddStock());
			}
		}

        public ICommand DeleteStoctCommand
        {
            get
            {
                return new RelayCommand(p => DeleteStock());
            }
        }

        public ICommand EditStockCommand
        {
            get
            {
                return new RelayCommand(p => DeleteStock());
            }
        }

        public ICommand DetailsStockCommand
        {
            get
            {
                return new RelayCommand(p => OpenDetailsStock());
            }
        }

        #endregion
    }
}
