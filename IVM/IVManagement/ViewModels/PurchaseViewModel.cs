namespace IVManagment.ViewModels
{
    using IVManagement.Data;
    using IVManagement.Helps;
    using IVManagement.ViewModels.Base;
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
	
	public class PurchaseViewModel : ViewModel
	{
		#region private proprieties
		
		private ObservableCollection<Purchases> purchasesList;
		private Purchases selectedPurchase;
		private string selectedPaymethod;
		#endregion

		#region public proprieties

        public DBInventoryEntities db;
		
		public List<string> Paymethods;
		#endregion

		#region Constructor
		
		public PurchaseViewModel()
		{
            Paymethods = new List<string>()
			{
				"Credit Card",
				"Visa Card", 
				"Direct Transaction", 
				"Paypal"
			};
			
			selectedPurchase = new Purchases();
            selectedPaymethod = "";
            LoadPurchases();
		}
		#endregion

		#region others proprieties
		
		public Purchases SelectedPurchase
		{
			get{return selectedPurchase;}
			set
			{
				selectedPurchase = value;
				NotifyPropertyChanged("SelectedPurchase");
			}
		}
		
		public ObservableCollection<Purchases> PurchasesList
		{
			get{return purchasesList;}
			set
			{
				purchasesList = value;
				NotifyPropertyChanged("PurchasesList");
			}
		}
		
		public string SelectedPayMethod
		{
			get{ return selectedPaymethod;}
			set
			{
				selectedPaymethod = value;
				NotifyPropertyChanged("SelectedPayMethod");
			}
		}
		
		public bool IsPayCommadValid
		{
			get
			{
				return true;
			}
		}
		#endregion

		#region private methods
		#endregion

		#region public methods
		
		public void LoadPurchases()
		{
			List<Purchases> purchases = new List<Purchases>();

            using (db = new DBInventoryEntities())
			{
				var loc_purchase = from p in db.Purchases where p.ProductId == 2 select p;

				foreach(Purchases p in loc_purchase)
				{
                    purchases.Add(p);
				}

                PurchasesList = new ObservableCollection<Purchases>(purchases);
			}
		}
		
		public void Pay()
		{
			switch(SelectedPayMethod)
			{
				case "Credit Card":
					PayWithCreditCard();
					break;
				case "Visa Card":
					PayWithVisaCard();
					break;
				case "Direct Transaction":
					PayThroughtDirectTransaction();
					break;
				case "Paypal":
					PayWithPaypal();
					break;
				default:
					PayThroughtDirectTransaction();
					break;
			}
		}
		
		public void getPurchasesByProductId()
		{
			
		}
		
		public void getPurchasesByCategoryId()
		{
			
		}
		
		public void SearchProductByProperties()
		{
			
		}
		
		public void PayWithCreditCard()
		{
			//
		}
		
		public void PayWithVisaCard()
		{
			//
		}
		
		public void PayThroughtDirectTransaction()
		{
			//
		}
		
		public void PayWithPaypal()
		{
			// CallLoginPaypal();
		}
		#endregion

		#region commands
		
		public ICommand PayCommand
		{
			get
			{
				return new RelayCommand(p => Pay(),
										p => IsPayCommadValid);
			}
		}
		#endregion
	}
}