﻿using IVManagement.Data;
using IVManagement.Helps;
using IVManagement.ViewModels.Base;
using IVManagement.Views;
using IVManagement.Views.UIControllers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace IVManagement.ViewModels
{
    public class EntriesViewModel : ViewModel
    {
        #region private proprieties

        private Products selectedProduct { get; set; }

        private Owners Owner { get; set; }
        //private EntriesView entriesView { get; set; }

        private string selectedCategory { get; set; }
        private List<string> searchfilter { get; set; }
        private ObservableCollection<Products> productsList { get; set; }

        private ObservableCollection<object> listobject { get; set; }

        #endregion

        #region public proprieties
        public DBInventoryEntities db;
        #endregion

        #region Constructor

        // public EntriesViewModel(EntriesView eView)
        public EntriesViewModel()
        {
            // entriesView = eView;
            Owner = new Owners(); // new
            selectedProduct = new Products();
            searchfilter = new List<string>();
            productsList = new ObservableCollection<Products>();
            listobject = new ObservableCollection<object>();
            LoadProduct();
        }
        #endregion

        #region others proprieties

        public List<string> Searchfilter
        {
            get { return searchfilter; }
            set
            {
                searchfilter = value;
                NotifyPropertyChanged("Searchfilter");
            }
        }

        public string SelectedCategory
        {
            get { return selectedCategory; }
            set
            {
                selectedCategory = value;
                NotifyPropertyChanged("SelectedCategory");
            }
        }

        public Products SelectedProduct
        {
            get { return selectedProduct; }
            set
            {
                selectedProduct = value;
                NotifyPropertyChanged("SelectedProduct");
            }
        }

        public ObservableCollection<Products> ProductsList
        {
            get { return productsList; }
            set
            {
                productsList = value;
                NotifyPropertyChanged("ProductsList");
            }
        }


        public ObservableCollection<object> Listobject
        {
            get { return listobject; }
            set
            {
                listobject = value;
                NotifyPropertyChanged("Listobject");
            }
        }


        #endregion

        #region private methods

        private object DeleteProduct()
        {
            throw new NotImplementedException();
        }

        private object OpenDetailsProduct()
        {
            throw new NotImplementedException();
        }

        private object EditProduct()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region public methods

        public void LoadProduct()
        {
            List<Products> loc_product = new List<Products>();
            List<Stocks> loc_stock = new List<Stocks>();

            List<object> loc_test = new List<object>();

            using (db = new DBInventoryEntities())
            {
                // get stock List

                // loc_stock = (from st in db.Stocks where st.OwnerId == entriesView.Owner.OwnerId select st).ToList();
                loc_stock = (from st in db.Stocks where st.OwnerId == Owner.OwnerId select st).ToList();

                // get number of products

                foreach (Stocks st in loc_stock)
                {
                    var items = (from p in db.Products
                                 where p.StockId == st.StockId
                                 select p).ToList();

                    foreach (Products p in items)
                    {
                        loc_product.Add(p);
                    }

                }
                ProductsList = new ObservableCollection<Products>(loc_product);

                foreach (Stocks st in loc_stock)
                {
                    var items = (from p in db.Products
                                 join pic in db.Pictures on p.PictureId equals pic.PictureId
                                 where p.StockId == st.StockId
                                 select new
                                 {
                                     ProductId = p.ProductId,
                                     StockId = p.StockId,
                                     CategoryId = p.CategoryId,
                                     PictureId = p.PictureId,
                                     Picture = pic.Productimage,
                                     Serialnumber = p.Serialnumber,
                                     Productname = p.Productname,
                                     Productmarke = p.Productmarke,
                                     Description = p.Description,
                                     Quantity = p.Quantiy,
                                     Unitprice = p.Unitprice,
                                     Inventoryprice = p.Inventoryprice,
                                     Saleprice = p.Saleprice,
                                     Purchasedate = p.Purchasedate,
                                     Startquantity = p.Startquantity,
                                     Statusquantity = p.Statusquantity,
                                     Statusorder = p.Statusorders
                                 }).ToList();

                    foreach (var p in items)
                    {
                        loc_test.Add(p);
                    }
                }
                Listobject = new ObservableCollection<object>(loc_test);

                Searchfilter.Add("refresh");

                Categories loc_category;
                foreach (Products pdt in ProductsList)
                {
                    loc_category = new Categories();
                    loc_category = (from c in db.Categories where c.CategoryId == pdt.CategoryId select c).First();

                    if (Searchfilter.Contains(loc_category.Categoryname))
                    {
                        // Do nothing !!!
                    }
                    else
                    {
                        Searchfilter.Add(loc_category.Categoryname);
                    }
                    
                }


            }
        }

        public void AddNewStock()
        {
            AddStockView stockview = new AddStockView(Owner);
            stockview.Show();
        }

        public void AddNewProduct()
        {
            AddProductView productview = new AddProductView(Owner);
            productview.Show();
        }
        #endregion

        #region commands

        public ICommand AddStockCommand
        {
            get
            {
                return new RelayCommand(p => AddNewStock());
            }
        }

        public ICommand AddProductCommand
        {
            get
            {
                return new RelayCommand(p => AddNewProduct());
            }
        }

        public ICommand DeleteProductCommand
        {
            get
            {
                return new RelayCommand(p => DeleteProduct());
            }
        }

        public ICommand EditProductCommand
        {
            get
            {
                return new RelayCommand(p => EditProduct());
            }
        }

        public ICommand DetailsProductCommand
        {
            get
            {
                return new RelayCommand(p => OpenDetailsProduct());
            }
        }

        
        #endregion
    }
}
