﻿using IVManagement.Data;
using IVManagement.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IVManagement.Models
{
    public class DBHandle : ViewModel
    {
        #region private proprieties

        // Address
        private Address address { get; set; }
        private Address selectedAdress { get; set; }
        private ObservableCollection<Address> adresslist { get; set; }

        // Customers
        private Customers customer { get; set; }
        private Customers selectedCustomer { get; set; }
        private ObservableCollection<Customers> customerslist { get; set; }

        // Suppliers
        private Suppliers supplier { get; set; }
        private Suppliers selectedSupplier { get; set; }
        private ObservableCollection<Suppliers> supplierslist { get; set; }

        // Users
        private Users user { get; set; }
        private Users selectedUser { get; set; }
        private ObservableCollection<Users> userslist { get; set; }

        // Owners
        private Owners owner { get; set; }
        private Owners selectedOwner { get; set; }
        private ObservableCollection<Owners> ownerslist { get; set; }

        // Orders
        private Orders order { get; set; }
        private Orders selectedOrder { get; set; }
        private ObservableCollection<Orders> orderslist { get; set; }

        // Sales
        private Sales sale { get; set; }
        private Sales selectedSale { get; set; }
        private ObservableCollection<Sales> saleslist { get; set; }

        // Stocks
        private Stocks stock { get; set; }
        private Stocks selectedStock { get; set; }
        private ObservableCollection<Stocks> stockslist { get; set; }

        // Categories
        private Categories category { get; set; }
        private Categories selectedCategory { get; set; }
        private ObservableCollection<Categories> categorieslist { get; set; }

        // Products
        private Products product { get; set; }
        private Products selectedProduct { get; set; }
        private ObservableCollection<Products> productslist { get; set; }

        // Purchases
        private Purchases purchase { get; set; }
        private Purchases selectedPurchase { get; set; }
        private ObservableCollection<Purchases> purchaseslist { get; set; }

        // Receipts
        private Receipts receipt { get; set; }
        private Receipts selectedReceipt { get; set; }
        private ObservableCollection<Receipts> receiptslist { get; set; }

        #endregion

        #region public proprieties
        #endregion

        #region Constructor

        public DBHandle()
        {

        }

        #endregion

        #region others proprieties

        // ################## Address #####################################

        public string StreetName
        {
            get { return address.Streetname; }
            set
            {
                address.Streetname = value;
                NotifyPropertyChanged("StreetName");
            }
        }

        public string HouseNumber
        {
            get { return address.Housenumber; }
            set
            {
                address.Housenumber = value;
                NotifyPropertyChanged("HouseNumber");
            }
        }

        public string PostalCode
        {
            get { return address.Postalcode; }
            set
            {
                address.Postalcode = value;
                NotifyPropertyChanged("PostalCode");
            }
        }

        public string City
        {
            get { return address.City; }
            set
            {
                address.City = value;
                NotifyPropertyChanged("City");
            }
        }

        public string Country
        {
            get { return address.Country; }
            set
            {
                address.Country = value;
                NotifyPropertyChanged("Country");
            }
        }

        public Address Address
        {
            get { return address; }
            set
            {
                address = value;
                NotifyPropertyChanged("Address");
            }
        }

        public Address SelectedAddress
        {
            get { return selectedAdress; }
            set
            {
                selectedAdress = value;
                NotifyPropertyChanged("SelectedAddress");
            }
        }

        public ObservableCollection<Address> Addresslist
        {
            get { return adresslist; }
            set
            {
                adresslist = value;
                NotifyPropertyChanged("Addresslist");
            }
        }

        // ##################################################################


        // ################ Customers #######################################

        public string CustomerTitle
        {
            get { return customer.Title; }
            set
            {
                customer.Title = value;
                NotifyPropertyChanged("CustomerTitle");
            }
        }

        public string CustomerFirstname
        {
            get { return customer.Firstname; }
            set
            {
                customer.Firstname = value;
                NotifyPropertyChanged("CustomerFirstname");
            }
        }

        public string CustomerLastname
        {
            get { return customer.Lastname; }
            set
            {
                customer.Lastname = value;
                NotifyPropertyChanged("CustomerLastname");
            }
        }

        public Customers Customer
        {
            get { return customer; }
            set
            {
                customer = value;
                NotifyPropertyChanged("Customer");
            }
        }

        public Customers SelectedCustomer
        {
            get { return selectedCustomer; }
            set
            {
                selectedCustomer = value;
                NotifyPropertyChanged("SelectedCustomer");
            }
        }

        public ObservableCollection<Customers> Customerslist
        {
            get { return customerslist; }
            set
            {
                customerslist = value;
                NotifyPropertyChanged("Customerslist");
            }
        }

        // #################################################################


        //####################### Suppliers  ###############################

        public string SupplierFirstname
        {
            get { return supplier.Firstname; }
            set
            {
                supplier.Firstname = value;
                NotifyPropertyChanged("SupplierFirstname");
            }
        }

        public string SupplierLastname
        {
            get { return supplier.Lastname; }
            set
            {
                supplier.Lastname = value;
                NotifyPropertyChanged("SupplierLastname");
            }
        }

        public string SupplierCompanyName
        {
            get { return supplier.Companyname; }
            set
            {
                supplier.Companyname = value;
                NotifyPropertyChanged("SupplierCompanyName");
            }
        }

        public Suppliers Supplier
        {
            get { return supplier; }
            set
            {
                supplier = value;
                NotifyPropertyChanged("Supplier");
            }
        }

        public Suppliers SelectedSupplier
        {
            get { return selectedSupplier; }
            set
            {
                selectedSupplier = value;
                NotifyPropertyChanged("SelectedSupplier");
            }
        }

        public ObservableCollection<Suppliers> Supplierslist
        {
            get { return supplierslist; }
            set
            {
                supplierslist = value;
                NotifyPropertyChanged("Supplierslist");
            }
        }

        // #######################################################################


        // ########################## Users #####################################

        public string UserName
        {
            get { return user.Username; }
            set
            {
                user.Username = value;
                NotifyPropertyChanged("UserName");
            }
        }

        public string PassWord
        {
            get { return user.Password; }
            set
            {
                user.Password = value;
                NotifyPropertyChanged("PassWord");
            }
        }

        public string UserType
        {
            get { return user.Usertype; }
            set
            {
                user.Usertype = value;
                NotifyPropertyChanged("UserType");
            }
        }

        public Users User
        {
            get { return user; }
            set
            {
                user = value;
                NotifyPropertyChanged("User");
            }
        }

        public Users SelectedUser
        {
            get { return selectedUser; }
            set
            {
                selectedUser = value;
                NotifyPropertyChanged("SelectedUser");
            }
        }

        public ObservableCollection<Users> Userslist
        {
            get { return userslist; }
            set
            {
                userslist = value;
                NotifyPropertyChanged("Userslist");
            }
        }

        // ###################################################################


        // ##################### Owners ######################################

        public string OwnerFirstname
        {
            get { return owner.Firstname; }
            set
            {
                owner.Firstname = value;
                NotifyPropertyChanged("OwnerFirstname");
            }
        }

        public string OwnerLastname
        {
            get { return owner.Lastname; }
            set
            {
                owner.Lastname = value;
                NotifyPropertyChanged("OwnerLastname");
            }
        }

        public string OwnerCompanyName
        {
            get { return owner.Companyname; }
            set
            {
                owner.Companyname = value;
                NotifyPropertyChanged("OwnerCompanyName");
            }
        }

        public Owners Owner
        {
            get { return owner; }
            set
            {
                owner = value;
                NotifyPropertyChanged("Owner");
            }
        }

        public Owners SelectedOwner
        {
            get { return selectedOwner; }
            set
            {
                selectedOwner = value;
                NotifyPropertyChanged("SelectedOwner");
            }
        }

        public ObservableCollection<Owners> Ownerslist
        {
            get { return ownerslist; }
            set
            {
                ownerslist = value;
                NotifyPropertyChanged("Ownerslist");
            }
        }

        // #########################################################################


        // #########################  Orders ########################################

        public Nullable<int> OrderQuantity
        {
            get { return order.Orderquantity; }
            set
            {
                order.Orderquantity = value;
                NotifyPropertyChanged("OrderQuantity");
            }
        }

        public Nullable<System.DateTime> OrderDate
        {
            get { return order.Orderdate; }
            set
            {
                order.Orderdate = value;
                NotifyPropertyChanged("OrderDate");
            }
        }

        public Nullable<System.DateTime> ShippedDate
        {
            get { return order.Shippeddate; }
            set
            {
                order.Shippeddate = value;
                NotifyPropertyChanged("ShippedDate");
            }
        }

        public string ShipVia
        {
            get { return order.Shipvia; }
            set
            {
                order.Shipvia = value;
                NotifyPropertyChanged("ShipVia");
            }
        }

        public Nullable<decimal> OrderAmount
        {
            get { return order.Amount; }
            set
            {
                order.Amount = value;
                NotifyPropertyChanged("OrderAmount");
            }
        }

        public Nullable<decimal> OrderVersement
        {
            get { return order.Versement; }
            set
            {
                order.Versement = value;
                NotifyPropertyChanged("OrderVersement");
            }
        }

        public Nullable<bool> PayStatus
        {
            get { return order.Paystatus; }
            set
            {
                order.Paystatus = value;
                NotifyPropertyChanged("PayStatus");
            }
        }

        public Orders Order
        {
            get { return order; }
            set
            {
                order = value;
                NotifyPropertyChanged("Order");
            }
        }

        public Orders SelectedOrder
        {
            get { return selectedOrder; }
            set
            {
                selectedOrder = value;
                NotifyPropertyChanged("SelectedOrder");
            }
        }

        public ObservableCollection<Orders> Orderslist
        {
            get { return orderslist; }
            set
            {
                orderslist = value;
                NotifyPropertyChanged("Orderslist");
            }
        }


        // ###########################################################################


        // ###################### Sales ##############################################

        public Nullable<System.DateTime> SaleDate
        {
            get { return sale.Saledate; }
            set
            {
                sale.Saledate = value;
                NotifyPropertyChanged("SaleDate");
            }
        }

        public Sales Sale
        {
            get { return sale; }
            set
            {
                sale = value;
                NotifyPropertyChanged("Sale");
            }
        }

        public Sales SelectedSale
        {
            get { return selectedSale; }
            set
            {
                selectedSale = value;
                NotifyPropertyChanged("SelectedSale");
            }
        }

        public ObservableCollection<Sales> Saleslist
        {
            get { return saleslist; }
            set
            {
                saleslist = value;
                NotifyPropertyChanged("Saleslist");
            }
        }


        // ###########################################################################


        // ######################### Stocks ##########################################

        public string StockName
        {
            get { return stock.Stockname; }
            set
            {
                stock.Stockname = value;
                NotifyPropertyChanged("StockName");
            }
        }

        public string StockSerialNumber
        {
            get { return stock.Serialnumber; }
            set
            {
                stock.Serialnumber = value;
                NotifyPropertyChanged("StockSerialNumber");
            }
        }

        public string StockCapacity
        {
            get { return stock.Capacity; }
            set
            {
                stock.Capacity = value;
                NotifyPropertyChanged("StockCapacity");
            }
        }

        public string StockLocation
        {
            get { return stock.Location; }
            set
            {
                stock.Location = value;
                NotifyPropertyChanged("StockLocation");
            }
        }

        public Stocks Stock
        {
            get { return stock; }
            set
            {
                stock = value;
                NotifyPropertyChanged("Stock");
            }
        }

        public Stocks SelectedStock
        {
            get { return selectedStock; }
            set
            {
                selectedStock = value;
                NotifyPropertyChanged("SelectedStock");
            }
        }

        public ObservableCollection<Stocks> Stockslist
        {
            get { return stockslist; }
            set
            {
                stockslist = value;
                NotifyPropertyChanged("Stockslist");
            }
        }


        // #############################################################################


        // ######################## Categories  ########################################

        public string CategoryName
        {
            get { return category.Categoryname; }
            set
            {
                category.Categoryname = value;
                NotifyPropertyChanged("CategoryName");
            }
        }

        public Categories Category
        {
            get { return category; }
            set
            {
                category = value;
                NotifyPropertyChanged("Category");
            }
        }

        public Categories SelectedCategory
        {
            get { return selectedCategory; }
            set
            {
                selectedCategory = value;
                NotifyPropertyChanged("SelectedCategory");
            }
        }

        public ObservableCollection<Categories> Categorieslist
        {
            get { return categorieslist; }
            set
            {
                categorieslist = value;
                NotifyPropertyChanged("Categorieslist");
            }
        }


        // ##############################################################################


        // ############################ Products  #######################################

        public string SerialNumber
        {
            get { return product.Serialnumber; }
            set
            {
                product.Serialnumber = value;
                NotifyPropertyChanged("SerialNumber");
            }
        }

        public string ProductName
        {
            get { return product.Productname; }
            set
            {
                product.Productname = value;
                NotifyPropertyChanged("Productname");
            }
        }

        public string ProductMarke
        {
            get { return product.Productmarke; }
            set
            {
                product.Productmarke = value;
                NotifyPropertyChanged("Productmarke");
            }
        }

        public string ProductDescription
        {
            get { return product.Description; }
            set
            {
                product.Description = value;
                NotifyPropertyChanged("ProductDescription");
            }
        }

        public Nullable<int> ProductQuantity
        {
            get { return product.Quantiy; }
            set
            {
                product.Quantiy = value;
                NotifyPropertyChanged("ProductQuantity");
            }
        }

        public Nullable<decimal> UnitPrice
        {
            get { return product.Unitprice; }
            set
            {
                product.Unitprice = value;
                NotifyPropertyChanged("UnitPrice");
            }
        }

        public Nullable<decimal> InventoryPrice
        {
            get { return product.Inventoryprice; }
            set
            {
                product.Inventoryprice = value;
                NotifyPropertyChanged("InventoryPrice");
            }
        }

        public Nullable<decimal> SalePrice
        {
            get { return product.Saleprice; }
            set
            {
                product.Saleprice = value;
                NotifyPropertyChanged("SalePrice");
            }
        }

        public Nullable<System.DateTime> InputsDate
        {
            get { return product.Purchasedate; }
            set
            {
                product.Purchasedate = value;
                NotifyPropertyChanged("InputsDate");
            }
        }

        public Nullable<int> StartQuantity
        {
            get { return product.Startquantity; }
            set
            {
                product.Startquantity = value;
                NotifyPropertyChanged("StartQuantity");
            }
        }

        public Nullable<int> StatusQuantity
        {
            get { return product.Statusquantity; }
            set
            {
                product.Statusquantity = value;
                NotifyPropertyChanged("StatusQuantity");
            }
        }

        public Nullable<int> StatusOrders
        {
            get { return product.Statusorders; }
            set
            {
                product.Statusorders = value;
                NotifyPropertyChanged("StatusOrders");
            }
        }

        public Products Product
        {
            get { return product; }
            set
            {
                product = value;
                NotifyPropertyChanged("Product");
            }
        }

        public Products SelectedProduct
        {
            get { return selectedProduct; }
            set
            {
                selectedProduct = value;
                NotifyPropertyChanged("SelectedProduct");
            }
        }

        public ObservableCollection<Products> Productslist
        {
            get { return productslist; }
            set
            {
                productslist = value;
                NotifyPropertyChanged("Productslist");
            }
        }


        // #####################################################################


        // #################### Purchases #####################################

        public Nullable<System.DateTime> PurchaseDate
        {
            get { return purchase.Purchasedate; }
            set
            {
                purchase.Purchasedate = value;
                NotifyPropertyChanged("PurchaseDate");
            }
        }

        public Purchases Purchase
        {
            get { return purchase; }
            set
            {
                purchase = value;
                NotifyPropertyChanged("Purchase");
            }
        }

        public Purchases SelectedPurchase
        {
            get { return selectedPurchase; }
            set
            {
                selectedPurchase = value;
                NotifyPropertyChanged("SelectedPurchase");
            }
        }

        public ObservableCollection<Purchases> Purchaseslist
        {
            get { return purchaseslist; }
            set
            {
                purchaseslist = value;
                NotifyPropertyChanged("Purchaseslist");
            }
        }

        // ####################################################################


        // ##################### Receipts #####################################

        public Receipts Receipt
        {
            get { return receipt; }
            set
            {
                receipt = value;
                NotifyPropertyChanged("Receipt");
            }
        }

        public Receipts SelectedReceipt
        {
            get { return selectedReceipt; }
            set
            {
                selectedReceipt = value;
                NotifyPropertyChanged("SelectedReceipt");
            }
        }

        public ObservableCollection<Receipts> Receiptslist
        {
            get { return receiptslist; }
            set
            {
                receiptslist = value;
                NotifyPropertyChanged("Receiptslist");
            }
        }

        // ##########################################################################
        #endregion

        #region private methods
        #endregion

        #region public methods
        #endregion

        #region commands
        #endregion
    }
}
