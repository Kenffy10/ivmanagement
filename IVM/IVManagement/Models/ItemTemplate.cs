﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace IVManagement.Models
{
    public class ItemTemplate
    {
        public Nullable<int> ProductId { get; set; }
        public Nullable<int> StockId { get; set; }
        public Nullable<int> CategoryId { get; set; }
        public Nullable<int> PictureId { get; set; }
        public byte[] Picture { get; set; }
        public string Serialnumber { get; set; }
        public string Productname { get; set; }
        public string Productmarke { get; set; }
        public string Description { get; set; }
        public Nullable<int> Quantity { get; set; }
        public Nullable<decimal> Unitprice { get; set; }
        public Nullable<decimal> Inventoryprice { get; set; }
        public Nullable<decimal> Saleprice { get; set; }
        public Nullable<System.DateTime> Purchasedate { get; set; }
        public Nullable<int> Startquantity { get; set; }
        public Nullable<int> Statusquantity { get; set; }
        public Nullable<int> Statusorder { get; set; }
    }
}
