﻿

namespace IVManagement.Helps
{

    using System;
    using System.Windows.Input;


    public sealed class RelayCommand : ICommand
    {
        readonly Action<object> action;
        readonly Predicate<object> predicate;

        //public event EventHandler CanExecuteChanged;

        public RelayCommand(Action<object> _action, Predicate<object> _predicate)
        {
            if (_action == null)
                throw new NullReferenceException("_action");

            this.action = _action;
            this.predicate = _predicate;
        }

        public RelayCommand(Action<object> _action)
            : this(_action, null)
        {

        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            if (predicate == null)
            {
                return true;
            }
            return predicate(parameter);
        }

        public void Execute(object parameter)
        {
            action(parameter);
        }

        public void Execute()
        {
            Execute(null);
        }
    }
}
