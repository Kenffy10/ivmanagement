﻿using IVManagement.Data;
using IVManagement.Views.UIControllers;
using IVManagment.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IVManagement.Views
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Owners Owner;

        public MainWindow(Owners owner)
        {
            InitializeComponent();
            Owner = new Owners();
            Owner = owner;
            DataContext = new MainWindowViewModel(Owner);
        }

    }
}
